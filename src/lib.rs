#![forbid(unsafe_code)]

mod evmap_impl;

pub use evmap_impl::{CacheReadHandle, LruCache};

/// Implementors of this trait expose a threadsafe LRU cache which keeps at most N items at a time,
/// specified in the parameter to `new`. There can be at most one thread doing writes (to the
/// struct impling this trait), and any number of readers, which can be created with `read_handle`.
pub trait ThreadsafeLru<K, V, ReadHandle: LruReadHandle<K, V>> {
    fn insert(&mut self, key: K, value: V);
    fn read_handle(&self) -> ReadHandle;
}

/// A read-only accessor to a [ThreadsafeLru].
pub trait LruReadHandle<K, V> {
    fn get(&self, key: &K) -> Option<V>;
    /// Returns the total size of the map
    fn len(&self) -> usize;
    /// Returns true if there are no entries in the map
    fn is_empty(&self) -> bool {
        self.len() == 0
    }
}
