# LRU Cache

This crate exposes a trait for a threadsafe LRU cache, and an implementation built on top of 
`evmap`. It can have at most one writer and any number of readers at a time. LRU stands for 
"least recently used", meaning once the map reaches capacity, the longest-ago accessed element
is removed in favor of new inserts.

Basic usage looks like:

```rust
let mut map = EvMapLruCache::new(5);
map.insert(1, "one");
assert_eq!(map.read_handle().len(), 1);
assert_eq!(map.read_handle().get(&1), Some("one"));
```

You can read from multiple threads by calling `read_handle()` and passing the result into the
threads that need to be able to read from the map.
